# Chad Launcher

The new version of Chad Launcher powered by [Tauri](https://tauri.studio).

Developed by the [GNU/Linux P2P Pirates](https://matrix.to/#/!SlYhhmreXjJylcsjfn:tedomum.net?via=matrix.org&via=tedomum.net) community and johncena141 release group from 1337x.

## Installation

### Build from source

We recommend using `pnpm` to build this project. ([AUR](https://aur.archlinux.org/packages/pnpm/))

```
pnpm install
pnpm build
pnpm tauri build
```

This will create a `chad_launcher` executable, an AppImage and a debian package.

## Development

### Running development server

```
pnpm dev
pnpm tauri dev
```

## Current GUI
